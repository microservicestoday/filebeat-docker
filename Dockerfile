FROM phusion/baseimage


#Installing Filebeat
ENV FILEBEAT_VERSION 5.0.2-linux-x86_64
ENV FILEBEAT_HOME /opt/filebeat
ENV FILEBEAT_PACKAGE filebeat-${FILEBEAT_VERSION}.tar.gz

RUN mkdir ${FILEBEAT_HOME} \
 && curl -O https://artifacts.elastic.co/downloads/beats/filebeat/${FILEBEAT_PACKAGE} \
 && tar xzf ${FILEBEAT_PACKAGE} -C ${FILEBEAT_HOME} --strip-components=1\
 && rm -f ${FILEBEAT_PACKAGE} \
 && groupadd -r filebeat \
 && useradd -r -s /usr/sbin/nologin -d ${FILEBEAT_HOME} -c "Filebeat service user" -g filebeat filebeat \
 && mkdir -p /var/log/filebeat /etc/filebeat/conf.d \
 && chown -R filebeat:filebeat ${FILEBEAT_HOME} /var/log/filebeat

ADD filebeat.yml ${FILEBEAT_HOME}
ADD start.sh ${FILEBEAT_HOME}
RUN chmod +x ${FILEBEAT_HOME}/start.sh
CMD ["sh","-c","${FILEBEAT_HOME}/start.sh"]
