# Filebeat for DCOS instances
This file beat created to track DC/OS logs based on [Log Management in DC/OS with ELK](https://docs.mesosphere.com/1.8/administration/logging/elk/) documentation.
#### Build and run
1. Make changes to the [config file](./filebeat.yml)
2. `docker build -t <image-name> .`
3. `docker run -d -e "LOGSTASH_URI=<logstash-uri>" -e "PRIVATE_IP=<private_ip>" -e "NODE_TYPE=<node_type>" <image-name>`

#### Pull and Run
1. `docker pull microservicestoday/filebeat`
2. `docker run -d -e "LOGSTASH_URI=<logstash-uri>" -e "PRIVATE_IP=<private_ip>" -e "NODE_TYPE=<node_type>" microservicestoday/filebeat`

####Note
- DC/OS logs can be distinguished based on the _type_ field:
    * **marathon_app** : Marathon application logs
    * **mesos_agent** : Mesos agent logs at _/var/log/mesos_
    * **mesos_master_journal** : DC/OS journalctl logs form master nodes
    * **mesos_agent_journal** : DC/OS journalctl logs form agent nodes

#### Tracking files
1. Mesos
    - "/var/log/mesos/\*.\*"
2. Mesos Journal logs
    - "/var/log/dcos/dcos.log"
3. Marathon app log
    - "/var/lib/mesos/slave/slaves/\*/frameworks/\*/executors/\*/runs/latest/stdout"
    - "/var/lib/mesos/slave/slaves/\*/frameworks/\*/executors/\*/runs/latest/stderr"
    
#### Contributing
1. Make a feature branch: __git checkout -b your-username/your-feature__
2. Follow Terraform Style Guide
3. Make your feature. Keep things tidy so you have one commit per self-contained change (squashing can help).
4. Push your branch: __git push -u origin your-username/your-feature__
5. Open GitHub to the repo,
   under "Your recently pushed branches", click __Pull Request__ for
   _your-username/your-feature_.

Be sure to use a separate feature branch and pull request for every
self-contained feature.  If you need to make changes from feedback, make
the changes in place rather than layering on commits (use interactive
rebase to edit your earlier commits).  Then use __git push --force
origin your-feature__ to update your pull request.
