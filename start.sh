#!/usr/bin/env bash
sed -i "s@LOGSTASH_URI@${LOGSTASH_URI}@g" ${FILEBEAT_HOME}/filebeat.yml
sed -i "s@NODE_TYPE@${NODE_TYPE}@g" ${FILEBEAT_HOME}/filebeat.yml
sed -i "s@PRIVATE_IP@${PRIVATE_IP}@g" ${FILEBEAT_HOME}/filebeat.yml
${FILEBEAT_HOME}/filebeat -c ${FILEBEAT_HOME}/filebeat.yml
